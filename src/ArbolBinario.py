class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz == None:
            self.raiz = Nodo(elemento)
        else:
            actual = self.raiz
            while True:
                if elemento < actual.valor:
                    if actual.izquierda == None:
                        actual.izquierda = Nodo(elemento)
                        break
                    else:
                        actual = actual.izquierda
                else:
                    if actual.derecha == None:
                        actual.derecha = Nodo(elemento)
                        break
                    else:
                        actual = actual.derecha

    def eliminar(self, elemento):
        pass

    def existe(self, elemento):
        actual = self.raiz
        while actual != None:
            if elemento == actual.valor:
                return True
            elif elemento < actual.valor:
                actual = actual.izquierda
            else:
                actual = actual.derecha
        return False

    def maximo(self):
        actual = self.raiz
        while actual != None and (actual.derecha != None or actual.izquierda != None):
            if actual.derecha != None:
                actual = actual.derecha
            else:
                actual = actual.izquierda
        if actual != None:
            return actual.valor
        else:
            return None

    def minimo(self):
        actual = self.raiz
        while actual != None and (actual.izquierda != None or actual.derecha != None):
            if actual.izquierda != None:
                actual = actual.izquierda
            else:
                actual = actual.derecha
        if actual != None:
            return actual.valor
        else:
            return None

    def altura(self):
        if self.raiz == None:
            return 0
        lista = [(self.raiz, 1)]
        altura = 0
        while lista != []:
            (actual, alt_actual) = lista.pop()
            altura = max(altura, alt_actual)
            if actual.izquierda != None:
                lista.append((actual.izquierda, alt_actual+1))
            if actual.derecha != None:
                lista.append((actual.derecha, alt_actual+1))
        return altura
    
    def obtener_elementos(self):
        elementos = []
        lista = []
        actual = self.raiz
        while actual != None or lista != []:
            while actual != None:
                lista.append(actual)
                actual = actual.izquierda
            actual = lista.pop()
            elementos.append(actual.valor)
            actual = actual.derecha
        return elementos

    def __str__(self):
        pass
